package com.example.saraujoc.myapplication;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQ_CODE_SPEECH_INPUT = 171;
    RecyclerView recyclerView;
    MyAdapter theAdapter;
    ZoomLayout container;
    EditText seekerText;
    ImageView micButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        container = (ZoomLayout) findViewById(R.id.activity_main);
        seekerText = (EditText) findViewById(R.id.seeker_edit_text);
        micButton = (ImageView) findViewById(R.id.mic_button);

        setRecycler();
        setMic();

    }

    private void setMic(){

        micButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMic();
            }
        });

    }

    private void startMic() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-CO");
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "es-CO");
        intent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, "es-CO");


        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "text to speech not supported",
                    Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    seekerText.setText(result.get(0));
                }
                break;
            }

        }
    }

    private void setRecycler() {
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        FixedGridLayoutManager layoutManager = new FixedGridLayoutManager();
        layoutManager.setTotalColumnCount(10);
        recyclerView.setLayoutManager(layoutManager);
        setAdapter();
    }

    private void setAdapter() {

        ArrayList<String> strings = new ArrayList<>();
        for (int i=0; i<100; i++){
            strings.add(String.valueOf(i));
        }
        theAdapter = new MyAdapter();
        theAdapter.setItems(strings);
        recyclerView.setAdapter(theAdapter);

    }


    private class MyAdapter extends RecyclerView.Adapter<MyHolder> {

        private ArrayList<String> items;

        void setItems(ArrayList<String> items) {
            this.items = items;
        }

        @Override
        public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
            return new MyHolder(view);
        }

        @Override
        public void onBindViewHolder(MyHolder holder, int position) {
            holder.bind(items.get(position));

        }

        @Override
        public int getItemCount() {
            return items == null ? 0 : items.size();
        }
    }

    private class MyHolder extends RecyclerView.ViewHolder implements ZoomLayout.ScaleListener {

        ImageView imageView;
        String descriptor;
        ZoomLayout layout;
        float scale;

        MyHolder(View itemView) {
            super(itemView);
            imageView= (ImageView) itemView.findViewById(R.id.customImageVIew);
            layout = (ZoomLayout) itemView.findViewById(R.id.item);

            layout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    layout.init(MainActivity.this);
                    return false;
                }
            });
            layout.setScaleListener(this);
        }

        void bind(String string) {
            descriptor = string;
        }

        @Override
        public void onScaleStart() {
            Log.i(TAG, "onScaleBegin");
        }

        @Override
        public void onScaleEnd() {
            Log.i(TAG, "onScaleEnd");
            if (scale > 1.75){
                nextLevel();
            } else if (scale < 0.6) {
                prevLevel();
            } else {
                Log.i(TAG, "not enough zoom");
                layout.scaleBy( 1/scale );
            }

        }

        private void nextLevel() {
            Log.i(TAG, "zooming to new level");
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
            container.startAnimation(animation);
        }

        @Override
        public void onScale(float scale) {
            Log.i(TAG, "onScale " + scale);
            this.scale = scale;
        }
    }

    private void prevLevel() {
        Log.i(TAG, "zooming to prev level");
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
        container.startAnimation(animation);

    }

}
